// AUTOR NATAN DE OLIVEIRA PEREIRA DA SILVA
// CONTATO NATAN673@GMAIL.COM

var MEDapp = angular.module('MEDapp', []);

MEDapp.controller('MEDappController', ['$scope', '$http', '$document',
	function ($scope, $http, $document) {
		$scope.textSize = 11;

		$scope.obterSimulados = function() {
			$http({
				method: 'GET',
				cache: false,
				url: "http://localhost:3000/includes/js/data/data.json",
			}).then(function (response) {
				$scope.listaSimulados = response.data.simulados;

				if (response.data.simulados.length > 1) {
					$scope.MSGquantidadeSimulados = "foram encontrados " + response.data.simulados.length + " registros";
				} else {
					$scope.MSGquantidadeSimulados = "foi encontrado " + response.data.simulados.length + " registro"
				}

				$scope.criarFiltros();
			}, function (error) {
				console.log("Algo aconteceu de errado !");
				console.log(error);               
			});
		}


		$scope.criarFiltros = function() {
		 var arr = [];
		 for (var i = 0; i < $scope.listaSimulados.length; i++) {
			if (!arr.includes($scope.listaSimulados[i].especialidade)) arr.push($scope.listaSimulados[i].especialidade);
		 }
		$scope.especialidadesFiltro = arr;

		var arr = [];
		for (var i = 0; i < $scope.listaSimulados.length; i++) {
			if (!arr.includes($scope.listaSimulados[i].turma)) arr.push($scope.listaSimulados[i].turma);
		}
		$scope.turmasFiltro = arr;

		var arr = [];
		for (var i = 0; i < $scope.listaSimulados.length; i++) {
			if (!arr.includes($scope.listaSimulados[i].filial)) arr.push($scope.listaSimulados[i].filial);
		}
		$scope.filiaisFiltro = arr;		
		}

		$scope.changeTextSize = function(simbolo){
			(simbolo == '+') ? $scope.textSize++ : $scope.textSize--
		}

		$scope.imprimir = function () {
			var printContents = document.getElementsByClassName('tabelaSimulados')[0].innerHTML;
			var popupWin = window.open('', '_blank', 'width=300,height=300');
			popupWin.document.open();
			popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="includes/css/master.css" /></head><body onload="window.print()"><table class="tabelaSimulados">' + printContents + '</table></body></html>');
			popupWin.document.close();
		}
	}]);


MEDapp.directive('lightboxDirective', function () {
	return {
		restrict: 'E',
		transclude: true, 
		template: '<section ng-transclude ng-click="showLightBox = false"></section>',
	}
})

